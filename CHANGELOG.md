# Change Log

## [v0.1.33](https://github.com/coder/vscode-coder/releases/tag/v0.1.33) (2024-02-20)

### Bug fixes

- Prevent updating template when automatically starting workspace.
